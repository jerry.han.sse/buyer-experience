# NOTE: Static Sites often want to optimize for fast build and deploy
#       pipeline times, so changes are published as quickly as possible.
#       Therefore, this default config includes optional variables, settings,
#       and caching, which help minimize job run times. For example,
#       disabling support for git LFS and submodules. There are also retry
#       and reliability settings which help prevent false build failures
#       due to occasional infrastructure availability problems. These are
#       all documented inline below, and can be changed or removed as
#       necessary, depending on the requirements for your repo or project.
include:
  template: Security/Dependency-Scanning.gitlab-ci.yml

stages:
  - prepare
  - lint
  - build
  - test
  - version
  - deploy

default:
  image: node:16-alpine
  interruptible: true # All jobs are interruptible by default
  # The following 'retry' configuration settings may help avoid false build failures
  # during brief problems with CI/CD infrastructure availability
  retry:
    max: 2 # This is confusing but this means "3 runs at max".
    when:
      - unknown_failure
      - api_failure
      - runner_system_failure
      - job_execution_timeout
      - stuck_or_timeout_failure

variables:
  # NOTE: The following PERFORMANCE and RELIABILITY variables are optional, but may
  #       improve performance of larger repositories, or improve reliability during
  #       brief problems with CI/CD infrastructure availability

  ### PERFORMANCE ###
  # GIT_* variables to speed up repo cloning/fetching
  GIT_DEPTH: 10
  # Disabling LFS and submodules will speed up jobs, because runners don't have to perform
  # the submodule steps during repo clone/fetch. These settings can be deleted if you are using
  # LFS or submodules.
  GIT_LFS_SKIP_SMUDGE: 1
  GIT_SUBMODULE_STRATEGY: none

  ### RELIABILITY ###
  # Reduce potential of flaky builds via https://docs.gitlab.com/ee/ci/yaml/#job-stages-attempts variables
  GET_SOURCES_ATTEMPTS: 3
  ARTIFACT_DOWNLOAD_ATTEMPTS: 3
  RESTORE_CACHE_ATTEMPTS: 3
  EXECUTOR_JOB_SECTION_ATTEMPTS: 3

### COMMON JOBS REUSED VIA `extends`:
.deployment-image:
  # Deployments need to use the Google Cloud SDK. This is why we split it out from the build job,
  # we used to use `google/cloud-sdk:latest`, but in order to patch gsutil to gzip files,
  # we are using the `www-gitlab-com` image. The imporant bit happens in this MR: https://gitlab.com/gitlab-org/gitlab-build-images/-/merge_requests/472,
  # which should allow us to complete the work in https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/122
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:www-gitlab-com-3.0

###################################
#
# PREPARE STAGE
#
###################################
before_script:
  - apk add --update curl && rm -rf /var/cache/apk/*

node-push-cache:
  stage: prepare
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
  cache:
    # Only push the cache from this job, to save time on all other jobs.
    policy: pull-push
  script:
    - echo "Pushing updated node cache..."

install:
  stage: .pre
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  script:
    - echo 'yarn-offline-mirror ".yarn-cache/"' >> .yarnrc
    - echo 'yarn-offline-mirror-pruning true' >> .yarnrc
    - yarn install --frozen-lockfile --no-progress
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - .yarn-cache/
  artifacts:
    paths:
      - node_modules/
      - content/synced

###################################
#
# LINT STAGE
#
###################################

js:
  stage: lint
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
  needs:
    - job: install
      artifacts: true
  script:
    - yarn lint:js

prettier:
  stage: lint
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
  needs:
    - job: install
      artifacts: true
  script:
    - yarn lint:prettier

###################################
#
# BUILD STAGE
#
###################################

build:
  stage: build
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
  artifacts:
    expire_in: 7 days
    paths:
      - dist
  needs: ['install']
  script:
    - yarn generate

###################################
#
# TEST STAGE
#
###################################

test:
  stage: test
  rules:
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"'
  script:
    - yarn install
    - yarn run test

scan-for-broken-links:
  stage: test
  needs:
    - job: install
      artifacts: true
    - job: build
      artifacts: true
  dependencies:
    - install
    - build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
  script:
    - yarn find-broken-links

###################################
#
# VERSION STAGE
#
###################################

version:
  stage: version
  before_script:
    - apk add --no-cache git
    - apk add --update --no-cache curl
  script:
    - yarn install
    - yarn semantic-release
  allow_failure: true
  variables:
    GL_TOKEN: $GITLAB_PROJECT_ACCESS_TOKEN
  only:
    variables:
      - $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
  except:
    refs:
      - tags
    variables:
      - $CI_COMMIT_TITLE =~ /^RELEASE:.+$/
  when: manual

###################################
#
# DEPLOY STAGE
#
###################################

# Review app logic for merge requests.
review:
  stage: deploy
  extends: .deployment-image
  needs:
    - job: build
      artifacts: true
  # The build job is listed as a dependency, so we will always have its artifacts available here,
  # and we can focus solely on executing deployment logic for the review apps
  dependencies:
    - build
  rules:
    # Run this job on merge requests, ONLY for a branch in the original repo.
    - if: '$CI_MERGE_REQUEST_IID && $CI_MERGE_REQUEST_SOURCE_PROJECT_ID == "28847821"'
  variables:
    DEPLOY_TYPE: review # This variable is used in the `scripts/deploy` bash file to switch on deployment logic.
  # Creates the review app environment, named with the slug for the commit ref, along with a corresponding URL that should match the deploy logic.
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: https://$CI_COMMIT_REF_SLUG.about.gitlab-review.app
    auto_stop_in: 30 days
  script:
    - mv dist/ public # The artifacts from the build job we care about are built to `dist/` by default in Nuxt, move to `public/`
    - scripts/deploy

pages:
  stage: deploy
  rules:
    # Only run the deploy on the default branch. If you want to deploy from Merge Request branches,
    # you can use Review Apps: https://docs.gitlab.com/ee/ci/review_apps
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
  needs:
    - build
  dependencies:
    - build
  script:
    - mv dist/ public
  artifacts:
    expire_in: 7 days
    paths:
      - public

production-deploy:
  stage: deploy
  extends: .deployment-image
  needs:
    - build
  # The build job is listed as a dependency, so we will always have its artifacts available here,
  # and we can focus solely on executing deployment logic
  dependencies:
    - build
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
  variables:
    DEPLOY_TYPE: production # This variable is used in the `scripts/deploy` bash file to switch on deployment logic.
  script:
    - mv dist/ public # The artifacts from the build job we care about are built to `dist/` by default in Nuxt, move to `public/`
    - scripts/deploy
