import Vue from 'vue';
import Components from 'slippers-ui';

// Register Slipper components with Vue
Object.keys(Components).forEach((name) => {
  Vue.component(name, Components[name]);
});
